/*
 * Encog(tm) Java Examples v3.4
 * http://www.heatonresearch.com/encog/
 * https://github.com/encog/encog-java-examples
 *
 * Copyright 2008-2017 Heaton Research, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *   
 * For more information on Heaton Research copyrights, licenses 
 * and trademarks visit:
 * http://www.heatonresearch.com/copyright
 */
package xor;

import org.encog.ConsoleStatusReportable;
import org.encog.Encog;
import org.encog.bot.BotUtil;
import org.encog.ml.MLRegression;
import org.encog.ml.data.MLData;
import org.encog.ml.data.versatile.NormalizationHelper;
import org.encog.ml.data.versatile.VersatileMLDataSet;
import org.encog.ml.data.versatile.columns.ColumnDefinition;
import org.encog.ml.data.versatile.columns.ColumnType;
import org.encog.ml.data.versatile.sources.CSVDataSource;
import org.encog.ml.data.versatile.sources.VersatileDataSource;
import org.encog.ml.factory.MLMethodFactory;
import org.encog.ml.model.EncogModel;
import org.encog.util.csv.CSVFormat;
import org.encog.util.csv.ReadCSV;
import org.encog.util.simple.EncogUtility;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

/**

 */
public class XORCSV {

	public static String DATA_URL = "C:\\Users\\lucas.pereira\\IdeaProjects\\Projeto-IA-MLP-Autism\\src\\main\\java\\dataset\\csv_result-Autism-Child-Data.csv";


     public void run(String[] args){

        try {
            // Carregando o arquivo...
            File autismFile = new File(XORCSV.DATA_URL);
            System.out.println("Downloading Autism dataset to: " + autismFile);


            //Definição da modelagem dos dados... suas colunas no CSV
            VersatileDataSource source = new CSVDataSource(autismFile, false, CSVFormat.ENGLISH);
            VersatileMLDataSet data = new VersatileMLDataSet(source);
            data.defineSourceColumn("id", 0, ColumnType.continuous);
            data.defineSourceColumn("A1_Score", 1, ColumnType.continuous);
            data.defineSourceColumn("A2_Score", 2, ColumnType.continuous);
            data.defineSourceColumn("A3_Score", 3, ColumnType.continuous);
            data.defineSourceColumn("A4_Score", 4, ColumnType.continuous);
            data.defineSourceColumn("A5_Score", 5, ColumnType.continuous);
            data.defineSourceColumn("A6_Score", 6, ColumnType.continuous);
            data.defineSourceColumn("A7_Score", 7, ColumnType.continuous);
            data.defineSourceColumn("A8_Score", 8, ColumnType.continuous);
            data.defineSourceColumn("A9_Score", 9, ColumnType.continuous);
            data.defineSourceColumn("A10_Score", 10, ColumnType.continuous);
            data.defineSourceColumn("age",       11, ColumnType.continuous);
            data.defineSourceColumn("gender",    12, ColumnType.nominal);
            data.defineSourceColumn("ethnicity", 13, ColumnType.nominal);
            data.defineSourceColumn("jundice",   14, ColumnType.nominal);
            data.defineSourceColumn("austim",    15, ColumnType.nominal);
            data.defineSourceColumn("contry_of_res",   16, ColumnType.nominal);
            data.defineSourceColumn("used_app_before", 17, ColumnType.nominal);
            data.defineSourceColumn("result",    18, ColumnType.continuous);
            data.defineSourceColumn("age_desc",  19, ColumnType.nominal);
            data.defineSourceColumn("relation",  20, ColumnType.nominal);
//            data.defineSourceColumn("Class_ASD", 21, ColumnType.continuous); //está classe será de predição


            // Define a coluna que estamos tentando prever...
            ColumnDefinition outputColumn = data.defineSourceColumn("Class_ASD", 21, ColumnType.nominal);

            // Analise os dados, determina o min/max/mean/sd de cada coluna.
            data.analyze();

            // Mapeie a coluna de previsão para a saída do modelo e todos
            // outras colunas para a entrada.
            data.defineSingleOutputOthersInput(outputColumn);

            // Cria uma rede Neural do modelo MPL que neste framework é XOR feedforward neural network as the model type. MLMethodFactory.TYPE_FEEDFORWARD.
            // Podemos escolher outro modelo para treinar a rede também, que são:
            // MLMethodFactory.SVM:  Support Vector Machine (SVM)
            // MLMethodFactory.TYPE_RBFNETWORK: RBF Neural Network
            // MLMethodFactor.TYPE_NEAT: NEAT Neural Network
            // MLMethodFactor.TYPE_PNN: Probabilistic Neural Network
            EncogModel model = new EncogModel(data);
            model.selectMethod(data, MLMethodFactory.TYPE_FEEDFORWARD);

            //fazendo a saida do console
            model.setReport(new ConsoleStatusReportable());

            //Agora normalizar os dados, a biblioteca faz isso automaticamente, determinando a correta normalização
            //com tipo base do que foi ecolhido no passa anterior...
            data.normalize();

            //Reter alguns dados para validação...
            // Irá embalhar os dados de forma aleatoria
            // Será usado um seed de 1001 para obter uma retenção de resultados mais consistente...
            model.holdBackValidation(0.3, true, 1001);

            //escolher o tipo de treinamento para este modelo..
            model.selectTrainingType(data);

            //vamos usar 5 fold com treinamento cross-validation, e retornar o melhor metodo encontrado..
            MLRegression bestMethod = (MLRegression) model.crossvalidate(5, true);

            //exibir o treianmento e validação de erros...
            System.out.println("Erros no Treinamento: " + EncogUtility.calculateRegressionError(bestMethod, model.getTrainingDataset()));
            System.out.println("Validacao Erro: " + EncogUtility.calculateRegressionError(bestMethod, model.getValidationDataset()));

            // exibir a normalização  dos parametros..
            NormalizationHelper helper = data.getNormHelper();
            System.out.print(helper.toString());

            //mostrar o modelo final...
            System.out.println("Modelo Final: " + bestMethod);


            // Faz um loop sobre o conjunto de dados original e o alimenta pelo modelo.
            // Isso também mostra como você processaria novos dados, que não faziam parte do seu conjunto de treinamento..
            // Você não precisa treinar, basta usar o NormalizationHelper Classe.
            // Depois de treinar, você pode salvar o NormalizationHelper para mais tarde usar...
            // normalize e desnormalize seus dados.
            ReadCSV csv = new ReadCSV(autismFile, false, CSVFormat.ENGLISH);
            String[] line = new String[22];
            MLData input = helper.allocateInputVector();

            while(csv.next()) {
                StringBuilder result = new StringBuilder();
                line[0] = csv.get(0);
                line[1] = csv.get(1);
                line[2] = csv.get(2);
                line[3] = csv.get(3);
                line[4] = csv.get(4);
                line[5] = csv.get(5);
                line[6] = csv.get(6);
                line[7] = csv.get(7);
                line[8] = csv.get(8);
                line[9] = csv.get(9);
                line[10] = csv.get(10);
                line[11] = csv.get(11);
                line[12] = csv.get(12);
                line[13] = csv.get(13);
                line[14] = csv.get(14);
                line[15] = csv.get(15);
                line[16] = csv.get(16);
                line[17] = csv.get(17);
                line[19] = csv.get(19);
                line[20] = csv.get(20);

                String correct = csv.get(21);
                helper.normalizeInputVector(line,input.getData(),false);
                MLData output = bestMethod.compute(input);
                String autismChosen = helper.denormalizeOutputVectorToString(output)[0];

                result.append(Arrays.toString(line));
                result.append(" -> Predicao: ");
                result.append(autismChosen);
                result.append("(correto: ");
                result.append(correct);
                result.append(")");

                System.out.println(result.toString());
            }

            // Finaliza.
            Encog.getInstance().shutdown();


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

	public static void main(String args[]) {

        XORCSV prg = new XORCSV();
        prg.run(args);

	}
}
